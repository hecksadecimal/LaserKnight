import Ragnarok
import random
import winlosescreen
import levelbase
import HeroTile
import DataParser

class Gamebase(object):
    def __init__(self):
        self.levelBinder = levelbase.LevelProgressManager()

        tiles = {}
        tiles["0"] = ["Transparent"]
        tiles["1"] = ["Solid"]
        tiles["2"] = ["Hazard"]
        tiles["3"] = ["LevelSwitch"]
        tiles["r"] = ["Reload"]

        # Load all needed levels. levelCount is the number of levels to be loaded, one-indexed
        levelCount = 7
        for i in range(levelCount):     #Checks for correct files
            levelPath = "..//maps//level" + str(i) + "//Data.txt"
            data = DataParser.parse(levelPath)
            prefix = "..//maps//level" + str(i) + "/Lv" + str(i)
            tile = prefix + "_TileMap.txt"
            collision = prefix + "_CollisionMap.txt"
            object = prefix + "_ObjectMap.txt"
            level = "level " + str(i)
            self.levelBinder.levels.append(level)
            tileMap = Ragnarok.SpriteSheet()
            tileMap.load_texture("..//textures//" + data[0], cell_size= Ragnarok.Vector2(64, 64))
            _map = Ragnarok.TileMap(tileMap, tiles, tile, collision, object, levelbase.ObjAry, level) #loads all arguements into TitleMap
            Ragnarok.TileMapManager.add_map(_map)   #adds all arguements to the add_map function

        # Testing levels
        levelTest = 0
        self.levelBinder.currentLevel = (levelTest -1)
        self.levelBinder.loadNextLevel()

        # Create hero
        self.hero = levelbase.loadCharacter(Ragnarok.TileMapManager.active_map, self)
        self.hero.setDefaultPos(Ragnarok.TileMapManager.active_map.start_location)
        Ragnarok.Ragnarok.get_world().add_obj(self.hero)