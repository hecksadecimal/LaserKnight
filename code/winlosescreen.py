import Ragnarok
import pygame
import os
from pygame.locals import*

fontPath = os.path.join("..", "fonts")
fontPath = os.path.join(fontPath, "foo.ttf")

class winScreen(Ragnarok.DrawableObj):
    def __init__(self):
        super(winScreen, self).__init__(self)
        img = pygame.image.load('..//textures//winscreen.png')
        screen= pygame.display.set_mode((1600,1600))
        running = 1
        while running:
            screen.blit(img,(1920, 1080))
            pygame.display.flip()
        self.winText = Ragnarok.Text(0,0, fontPath, 100, (255,255,255))
        self.winText.text = "You Win, Lazer Knight! Congratulations!"
        self.winText.coords = Ragnarok.Vector2(1920 / 2, 1080 / 2) #magic numbers!
        self.is_enabled = False
        self.is_visible = False

    def show(self):
        self.is_enabled = True
        self.is_visible = True

    def draw(self, milliseconds, surface):
        pygame.draw.rect(surface, (0,0,0,175)), pygame.Rect(0,0,1280,720) # magic numbers oh god
        self.winText.draw(milliseconds, surface)

class loseScreen(Ragnarok.DrawableObj):
    def __init__(self, char):
        global loseCount
        super(loseScreen, self).__init__(self)
        img = pygame.image.load('..//textures//losescreen.png')
        img = pygame.transform.scale(img,(1280,720))
        screen  = pygame.display.set_mode((1280, 720))
        running = 1
        while running:
            screen.blit(img,(0,0))
            pygame.display.flip()
        self.winText = Ragnarok.Text(0,0, fontPath, 84, (255,255,255))
        self.winText.text = "You Have Been Bested By the Evil Doctor!"
        self.winText.coords = Ragnarok.Vector2(1280 / 2, 720 / 2)
        self.respawnText = Ragnarok.Text(0,0, fontPath, 32, (255,255,255))
        self.respawnText.text = "Press space to respawn!"
        self.respawnText.coords = self.winText.coords + Ragnarok.Vector2(0,128)
        self.is_enabled = False
        self.is_visible = False
        self.char = char
        self.delayTimer = Ragnarok.Timer(500)

    def show(self):
        self.delayTimer.is_enabled = True   #sets delay between win/lose event and win/lose screen
        self.char.is_paused = True

    def update(self, milliseconds):     #respawning after character death
        if self.delayTimer.is_enabled:
            self.delayTimer.update(milliseconds)
            if self.delayTimer.is_ringing():
                self.is_enabled = True
                self.is_visible = True
                self.delayTimer.is_enabled = True
                self.delayTimer.reset()

        if self.is_enabled:
            if Ragnarok.Ragnarok.get_world().Keyboard.is_clicked(pygame.K_SPACE):
                self.char.currentState = self.char.normalState
                self.char.setDefaultPos(Ragnarok.TileMapManager.active_map.start_location)
                self.is_enabled = False
                self.is_visible = False
                self.char.is_paused = False
                Ragnarok.Ragnarok.get_world().remove_obj(self)


    def draw(self, milliseconds, surface):
        if self.is_visible:
            pygame.draw.rect(surface, (0,0,0,175), pygame.Rect(0,0,1280,960))
            self.winText.draw(milliseconds, surface)
            self.respawnText.draw(milliseconds, surface)
