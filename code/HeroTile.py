import Ragnarok
import pygame
import math

class HeroTile(Ragnarok.TileMapObject):
    def __init__(self, tileMap, GameScreen):
        super(HeroTile, self).__init__(tileMap)
        self.game_screen = GameScreen
        self.desiredPos = Ragnarok.Vector2(100, 20)
        self.jumpingState = "JUMPING"
        self.normalState = "NORMAL"
        self_isPaused = False
        self.currentState = self.normalState
        self.draw_order = 5000

        jumpHeightinBlocks = 3
        tileHeight = 64
        fallAccel = 32 * tileHeight

        # -vi = sqrt(3*accel*delta(y)
        self.jumpVelocity = math.sqrt(2*(fallAccel) * (jumpHeightinBlocks * tileHeight))
        self.runSpeed = 500

        self.hazardTouched = None
        self.isPaused = False

        self.boundingBox = Ragnarok.AABoundingBox()
        self.boundingBox = pygame.Rect(0,0,100,100) # What does the 100,100 correspond to?
        self.velocity = Ragnarok.Vector2()
        self.acceleration = Ragnarok.Vector2(0, fallAccel)
        Ragnarok.Ragnarok.get_world().CollisionMgr.add_object(self.boundingBox)

    def setDefaultPos(self, defaultPos):
        self.coords = defaultPos
        self.velocity = Ragnarok.Vector2()
        self.desiredPos = defaultPos
        Ragnarok.Ragnarok.get_world().Camera.pan = self.coords
        Ragnarok.Ragnarok.get_world().Camera.desired_pan = self.coords

    def __step(self,oldPos,desiredPos):
        self.coords = desiredPos
        collisions = Ragnarok.TileMapManager.active_map.grab_collisions(Ragnarok.Vector2(oldPos.X, self.coords.Y))
        checkHorizontal = True
        for collision in collisions:
            if "Solid" in collision.binding_type:
                self.coords.Y = oldPos.Y
                self.velocity.Y = 0
                if collision.coords.Y > self.coords.Y:
                    self.currentState = self.normalState
            elif "Hazard" in collision.binding_type:
                self.hazardTouched(self)
                checkHorizontal = False
                collisions = []
                break
            elif "LevelSwitch" in collision.binding_type:
                self.game_screen.levelBinder.loadNextLevel   ()
                self.setDefaultPos(Ragnarok.TileMapManager.active_map.start_location)
                checkHorizontal = False
                break
            elif "Reload" in collision.binding_type:
                self.setDefaultPos(Ragnarok.TileMapManager.active_map.start_location)
                checkHorizontal = False
        if checkHorizontal:
            collisions = Ragnarok.TileMapManager.active_map.grab_collisions(Ragnarok.Vector2(self.coords.X, oldPos.Y))
            for collision in collisions:
                if "Solid" in collision.binding_type:
                    self.coords.X = oldPos.X
                    self.velocity.X = 0
                elif "Hazard" in collision.binding_type:
                    self.hazardTouched(self)
                    collisions = []
                    break
                elif "Reload" in collision.binding_type:
                    self.setDefaultPos(Ragnarok.TileMapManager.active_map.start_location)
                    break

        self.desiredPos = self.coords

    def __moveUpdate(self):
        oldPos = self.coords.copy()
        prevPos = self.coords.copy()
        direction = self.desiredPos - oldPos
        yStepCount = int(abs(direction.Y) / 64) + 1
        xStepCount = int(abs(direction.X) / 64) + 1

        if yStepCount > 1:
            oldPos = self.tile_map.pixels_to_tiles(oldPos)
            oldPos = self.tile_map.tiles_to_pixels(oldPos)
            self.desiredPos.Y = oldPos.Y + (64 * Ragnarok.sign(direction.Y))


        if xStepCount > 1:
            oldPos = self.tile_map.pixels_to_tiles(oldPos)
            oldPos = self.tile_map.tiles_to_pixels(oldPos)
            self.desiredPos.X = oldPos.X + (64 * Ragnarok.sign(direction.X))

        self.__step(oldPos, self.desiredPos)

    def __handleInput(self, milliseconds):
        if Ragnarok.Ragnarok.get_world().Keyboard.is_down(pygame.K_d):
            self.desiredPos += Ragnarok.Vector2(self.runSpeed * (milliseconds / 1000.0), 0)
        if Ragnarok.Ragnarok.get_world().Keyboard.is_down(pygame.K_a):
            self.desiredPos -= Ragnarok.Vector2(self.runSpeed * (milliseconds / 1000.0), 0)
        if Ragnarok.Ragnarok.get_world().Keyboard.is_down(pygame.K_w):
            if self.currentState != self.jumpingState:
                self.velocity = Ragnarok.Vector2(0, -self.jumpVelocity)
                self.currentState = self.jumpingState
        if Ragnarok.Ragnarok.get_world().Keyboard.is_down(pygame.K_ESCAPE):
            pygame.quit()

    def update(self, milliseconds):
        if not self.isPaused:
            self.velocity += self.acceleration * (milliseconds / 1000.0)
            self.desiredPos += self.velocity * (milliseconds / 1000.0)
            self.__moveUpdate()
            self.__handleInput(milliseconds)
            self.boundingBox.x = self.coords.X
            self.boundingBox.y = self.coords.Y
            Ragnarok.Ragnarok.get_world().Camera.desired_pan = self.coords
            super(HeroTile, self).update(milliseconds)

    def draw(self, milliseconds, surface):
        if self.is_visible:
            surface.blit(self.image, self.coords)