import Ragnarok

class Key(Ragnarok.TileMapObject):
    def __init__(self, character, tileMap, winFunction):
        super(Key, self).__init__(tileMap)
        self.load_texture("power_up.png")
        self.origin = Ragnarok.Vector2(0,0)
        self.character = character
        self.winFunction = winFunction

    def checkCollisionWithPlayer(self):
        playerTiles = Ragnarok.TileMapManager.active_map.grab_collisions(self.character.coords)
        enemyTiles = Ragnarok.TileMapManager.active_map.grab_collisions(self.coords)

        # really narsty hack to check for same tiles
        for playerTile in playerTiles:
            for enemyTile in enemyTiles:
                if Ragnarok.TileMapManager.active_map.pixels_to_tiles(playerTile.coords) == Ragnarok.TileMapManager.active_map.pixels_to_tiles(enemyTile.coords):
                    return True

        return False

    # if colliding with player, remove object
    def update(self, milliseconds):
        if self.checkCollisionWithPlayer():
            self.winFunction()
            Ragnarok.Ragnarok.get_world().remove_obj(self)