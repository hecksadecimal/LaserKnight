import Ragnarok
import winlosescreen
import HeroTile

def hazardTouched(hero): # Freezes character and throws screen over them
    hero.currentState = hero.normalState
    screen = winlosescreen.loseScreen(hero)
    screen.show()
    screen.draw_order = 15000
    Ragnarok.Ragnarok.get_world().add_obj(screen)

def winTouched():   #freezes character and builds screen over them
    hero.currentState = hero.normalState
    screen - winlosescreen.winScreen()
    screen.show()
    screen.draw_order = 15000
    Ragnarok.Ragnarok.get_world().add_obj(screen)

def loadCharacter(tileMap, gamescreen):
    tileObj = HeroTile.HeroTile(tileMap, gamescreen)
    tileObj.load_texture("..//textures//hero.png")
    tileObj.origin = Ragnarok.Vector2(0,0)
    tileObj.hazardTouched = hazardTouched
    tileObj.winTouched = winTouched
    return tileObj

ObjAry = [] # Contains everything you might want to put on the map

class LevelProgressManager(object):
    def __init__(self):
        self.levels = []
        self.currentLevel = -1

    def loadNextLevel(self):
        self.currentLevel += 1
        if self.currentLevel > len(self.levels) - 1:
            self.currentLevel = 0

        if self.currentLevel == 5: #This will need to change depending on how many levels we have (Number of levels +1)
            gameWin = Ragnarok.Text(100,100, winlosescreen.fontPath, 16, (0,0,0)) #Will need to be scrapped and replaced with an image
            gameWin.text = "And with that, the Evil Spikes were defeated! Thank you for playing!"
            gameWin.coords = Ragnarok.Vector2(400,150) #will need to be scrapped as well
            gameWin.tag = "Winner Text"
            Ragnarok.Ragnarok.get_world().add_obj(gameWin)
        else:
            Ragnarok.Ragnarok.get_world().remove_all("Winner Text")

        Ragnarok.TileMapManager.load(self.levels[self.currentLevel]) #loads current level

    def loadPreviousLevel(self):    #exists as a precaution
        self.currentLevel -= 1
        Ragnarok.TileMapManager.load(self.levels[self.currentLevel])