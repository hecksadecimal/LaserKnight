import Ragnarok
import pygame
import gamebase
instance = Ragnarok.Ragnarok(Ragnarok.Vector2(1280,720), "Lazer Knight")
world = instance.get_world()
#Create an instance of our game, which will add components necessary to gameplay to the Ragnarok engine.
Game = gamebase.Gamebase()

pygame.mouse.set_visible(True) # May wish to change to False at the expo
instance.preferred_fps = 60
instance.print_frames = False

#Start the game loop.
instance.run()